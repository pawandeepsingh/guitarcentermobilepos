//
//  ProductDetailControllerViewController.m
//  GuitarCenter
//
//  Created by Pawandeep Singh on 1/6/16.
//  Copyright © 2016 Expicient Inc. All rights reserved.
//

#import "ProductDetailController.h"
#import "UIImageView+AFNetworking.h"
#import "Mantle.h"
#import "SearchResultsModel.h"

@interface ProductDetailController ()

@end

@implementation ProductDetailController

@synthesize item;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self intializeProductDetails];
}

-(void)intializeProductDetails {
    [_productNameLbl setText:item.primaryInformation.shortDescription];
    [self setTitle:item.displayItemId];
    
    NSString* imageURL = [NSString stringWithFormat:@"%@/%@",[item.primaryInformation imageLocation], [item.primaryInformation imageID]];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imageURL]];
    UIImage *placeholderImage =[UIImage imageNamed:@"gc-logo-transparent"];
    [_productImageView setImageWithURLRequest:request
                                 placeholderImage:placeholderImage
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              _productImageView.image = image;
                                              [_productImageView setNeedsLayout];
                                              
                                          } failure:nil];
    
    NSDictionary* dict = [MTLJSONAdapter JSONDictionaryFromModel:item error:nil];
    NSString* displayText = [dict description];
    displayText = [displayText stringByReplacingOccurrencesOfString:@"{" withString:@""];
    displayText = [displayText stringByReplacingOccurrencesOfString:@"}" withString:@""];
    displayText = [displayText stringByReplacingOccurrencesOfString:@"};" withString:@""];
    displayText = [displayText stringByReplacingOccurrencesOfString:@";" withString:@""];
    [_descriptionTextView setText:displayText];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
