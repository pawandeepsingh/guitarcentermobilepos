//
//  ProductList.h
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/29/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchResultsModel.h"

@interface ProductListController : UICollectionViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) SearchResultsModel *searchResults;
@property (strong, nonatomic) NSString* searchString;

@end
