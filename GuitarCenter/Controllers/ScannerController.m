//
//  ScannerController.m
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/24/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//

#import "ScannerController.h"
#import "VFDevice.h"
#import "Logger.h"

@implementation ScannerController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[VFDevice barcode] setDelegate:self];
}

-(void)appendDataToTextView:(NSString*)text {
    tv.text = [NSString stringWithFormat:@"%@\n%@", tv.text, text];
}

-(IBAction) scannerOn:(id)sender{
    [self appendDataToTextView:@"scannerOn: Starting scanning"];
    [[VFDevice barcode] startScan];
}
-(IBAction)scannerOff:(id)sender{
    [self appendDataToTextView:@"scannerOff: Scanning Abort"];
    [[VFDevice barcode] abortScan];
}
-(IBAction)scanner1DMode:(id)sender{
    [self appendDataToTextView:@"scanner1DMode"];
    [[VFDevice barcode] setScanner1D];
}
-(IBAction)scanner2DMode:(id)sender{
    [self appendDataToTextView:@"scanner2DMode"];
    [[VFDevice barcode] setScanner2D];
}
-(IBAction)triggerOn:(id)sender{
    [self appendDataToTextView:@"triggerOn"];
    [[VFDevice barcode] sendTriggerEvent:YES];
}
-(IBAction)triggerOff:(id)sender{
    [self appendDataToTextView:@"triggerOff"];
    [[VFDevice barcode] sendTriggerEvent:NO];
}
-(IBAction)setSoft:(id)sender{
    [self appendDataToTextView:@"setSoft"];
    [[VFDevice barcode] setSoft];
}
-(IBAction)setEdge:(id)sender{
    [self appendDataToTextView:@"setEdge"];
    [[VFDevice barcode] setEdge];
}
-(IBAction)setLevel:(id)sender{
    [self appendDataToTextView:@"setLevel"];
    [[VFDevice barcode] setLevel];
}
-(IBAction)setPassive:(id)sender{
    [self appendDataToTextView:@"setPassive"];
    [[VFDevice barcode] setPassive];
}
-(IBAction)beepOn:(id)sender{
    [self appendDataToTextView:@"beepOn"];
    [[VFDevice barcode] beepOnParsedScan:YES];
}
-(IBAction)beepOff:(id)sender{
    [self appendDataToTextView:@"beepOff"];
    [[VFDevice barcode] beepOnParsedScan:NO];
}

-(IBAction)clear:(id)sender{
    tv.text=@"";
}

-(void) barcodeConnected:(BOOL)isConnected{
    if(isConnected){
        [self appendDataToTextView:@"barcodeConnected: BARCODE CONNECTED"];
        connectedLabel.text = @"BARCODE CONNECTED";
        [connectedLabel setTextColor:[UIColor greenColor]];
    }
    else{
        [self appendDataToTextView:@"barcodeConnected: BARCODE DISCONNECTED"];
        connectedLabel.text = @"BARCODE DISCONNECTED";
        [connectedLabel setTextColor:[UIColor whiteColor]];
    }
}

-(void) barcodeScanData:(NSData *)data barcodeType:(int)thetype{
    NSString* barcode = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [self appendDataToTextView:[NSString stringWithFormat:@"barcodeScanData: Barcode: %@. BarcodeType: %d", barcode, thetype]];
}

//OPTIONAL BARCODE DELEGATE PROTOCOLS
-(void) commandResult:(int)result{
   DDLogInfo(@"commandResult: %d", result);
}

-(void) barcodeTriggerEvent:(int)BCS_TRIGGER{
    DDLogInfo(@"barcodeTriggerEvent: %d", BCS_TRIGGER);
}

-(void) barcodeLogEntry:(NSString *)logEntry withSeverity:(int)severity{
    DDLogInfo(@"barcodeLogEntry: %@. Sevirity: %d", logEntry, severity);
}

-(void) barcodeSerialData:(NSData *)data incoming:(BOOL)isIncoming {
    NSString *barcode;
    if(isIncoming){
        barcode = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [self appendDataToTextView:[NSString stringWithFormat:@"Incoming barcodeSerialData: Barcode: %@", barcode]];
    } else {
        barcode = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [self appendDataToTextView:[NSString stringWithFormat:@"Outgoing barcodeSerialData: Barcode: %@", barcode]];
    }
}

-(void) barcodeInitialized:(BOOL)isInitialized{
    if(isInitialized) {
        [self appendDataToTextView:@"Barcode Intialized"];
        [[VFDevice barcode] setScanner2D];
        [[VFDevice barcode] setScanTimeout:5000];
        [[VFDevice barcode] includeAllBarcodeTypes];
        [[VFDevice barcode] barcodeTypeEnabled:TRUE];
    } else {
        [self appendDataToTextView:@"Barcode not Intialized"];
    }
}

-(void) barcodeReconnectStarted{
    [self appendDataToTextView:@"barcodeReconnectStarted"];
}

-(void) barcodeReconnectFinished{
    [self appendDataToTextView:@"barcodeReconnectFinished"];
}

-(void) barcodeDataReceived:(NSData *)data{
     [self appendDataToTextView:@"barcodeReconnectFinished"];
}

-(void) barcodeDataSent:(NSData *)data{
    NSString* barcode = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [self appendDataToTextView:[NSString stringWithFormat:@"barcodeScanData: Barcode: %@", barcode]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
