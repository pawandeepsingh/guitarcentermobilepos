//
//  ProductDetailControllerViewController.h
//  GuitarCenter
//
//  Created by Pawandeep Singh on 1/6/16.
//  Copyright © 2016 Expicient Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchResultsModel.h"

@interface ProductDetailController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *productNameLbl;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;

@property (strong, nonatomic) Item *item;

@end
