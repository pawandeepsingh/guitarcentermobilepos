//
//  ScannerController.h
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/24/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <VMF/VMFramework.h>

@interface ScannerController : UIViewController <VFIBarcodeDelegate> {
    IBOutlet UITextView *tv;
    IBOutlet UILabel *connectedLabel;
}

- (IBAction)scannerOn:(id)sender;
- (IBAction)scannerOff:(id)sender;
- (IBAction)scanner1DMode:(id)sender;
- (IBAction)scanner2DMode:(id)sender;
- (IBAction)triggerOn:(id)sender;
- (IBAction)triggerOff:(id)sender;
- (IBAction)setSoft:(id)sender;
- (IBAction)setPassive:(id)sender;
- (IBAction)setEdge:(id)sender;
- (IBAction)setLevel:(id)sender;
- (IBAction)beepOn:(id)sender;
- (IBAction)beepOff:(id)sender;
- (IBAction)clear:(id)sender;

@end
