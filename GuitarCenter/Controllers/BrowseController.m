//
//  FirstViewController.m
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/21/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//

#import "BrowseController.h"
#import "SearchResultsModel.h"
#import "Logger.h"
#import "AFNetworking.h"
#import "Mantle.h"
#import "SterlingClient.h"
#import "MBProgressHUD.h"
#import "ProductListController.h"
#import "ProductDetailController.h"
#import "SearchResultsModel.h"
#import "VFDevice.h"
#import "AFNetworking.h"

@interface BrowseController () {
    MBProgressHUD *HUD;
}
@end

@implementation BrowseController

static BOOL isScanning = NO;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[VFDevice barcode] setDelegate:self];
    [_searchTextFld addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_goBtn setEnabled:NO];
    [_goBtn setBackgroundColor:[UIColor grayColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)scanTapped:(id)sender {
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    
    
    HUD.labelText = @"Now press and hold the triggers* to scan";
    HUD.detailsLabelText = @"Triggers: Button's on the back of the device";
    HUD.dimBackground = YES;
    
    [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
    [self startScan];
}

- (void)showAlert:(NSString*)title withMessage:(NSString*)message withBtnTitle:(NSString*)btnTitle {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    //We add buttons to the alert controller by creating UIAlertActions:
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:btnTitle
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alertController addAction:actionOk];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)search:(NSString*)searchText{
    if (searchText==nil || [searchText length] == 0) {
        searchText = @"";
    }
    NSString *searchTerm = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    DDLogInfo(@"BrowseController: Search String: %@", searchText);
    
    SterlingClient *client = [SterlingClient manager];
    
    NSString* apiMetaParamValue = @"{\"appName\":\"poc\",\"apiName\":\"searchCatalogIndex\",\"isService\":false}";
    NSString *inputMetaParamValue = [NSString stringWithFormat:@"{\"CatalogSearch\":{\"CallingOrganizationCode\":\"GCI\",\"Terms\":{\"Term\":{\"Condition\":\"MUST\",\"Value\":\"%@\"}}}}", searchTerm];
    
    NSDictionary *params = @{
                             @"apimeta": apiMetaParamValue,
                             @"input": inputMetaParamValue
                             };
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.mode = MBProgressHUDModeIndeterminate;
    
    
    HUD.labelText = @"Loading...";
    HUD.detailsLabelText = [NSString stringWithFormat:@"Results for \"%@\"",searchTerm];
    HUD.dimBackground = YES;
    
    [client POST:@"http://10.132.5.68:8080/storeui/api/store/api"
      parameters:params
      completion:^(OVCResponse *response, NSError *error) {
          [MBProgressHUD hideHUDForView:self.view animated:YES];
          
          SearchResultsModel *searchResultModel = [MTLJSONAdapter modelOfClass:[SearchResultsModel class] fromJSONDictionary:response.rawResult error:&error];
          
          if (searchResultModel!=nil) {
              if([searchResultModel.status integerValue] == 200) {
                  DDLogInfo(@"Success!!!");
                  
                  UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main"
                                                               bundle:nil];
                  if ([searchResultModel.results.catalogSearch.totalHits integerValue] == 0) {
                      [self showAlert:@"Information" withMessage:@"No results found for search term \"%@\". Please try another term." withBtnTitle:@"Ok"];
                  } else if ([searchResultModel.results.catalogSearch.totalHits integerValue] == 1) {
                      ProductDetailController *pdCtrl  = [sb instantiateViewControllerWithIdentifier:@"ProductDetailController"];
                      Item* item = [searchResultModel.results.catalogSearch.itemList.items objectAtIndex:0];
                      [pdCtrl setItem:item];
                      [self.navigationController pushViewController:pdCtrl animated:YES];
                  } else {
                      ProductListController *plCtrl  = [sb instantiateViewControllerWithIdentifier:@"ProductListController"];
                      [plCtrl setSearchResults:searchResultModel];
                      [plCtrl setSearchString:searchTerm];
                      [self.navigationController pushViewController:plCtrl animated:YES];
                  }
              } else {
                  [self showAlert:@"Error" withMessage:@"Server Returned an error. Please retry again later." withBtnTitle:@"Ok"];
              }
          } else {
              NSString *errStr = nil;
              if(error!=nil && error.userInfo!=nil) {
                  errStr =  [NSString stringWithString:[error.userInfo valueForKey:@"NSLocalizedDescription"]];
                  if(errStr==nil || [errStr length] == 0) {
                      errStr = @"The request timed out.";
                  }
              } else {
                  errStr = [NSString stringWithFormat:@"No results found for search term \"%@\". Please try another term.", searchTerm];
              }
              
              [self showAlert:@"Information" withMessage:errStr withBtnTitle:@"Ok"];
              
              DDLogError(@"Failure %@", [error description]);
          }
      }];
}

- (IBAction)goTapped:(id)sender {
    NSString* searchStr = [_searchTextFld text];
    DDLogInfo(@"Search String: %@", searchStr);
    [self search:searchStr];
}

- (void) barcodeScanData: (NSData *)data barcodeType:(int)thetype{
    NSString* barcode = [[NSString alloc] initWithData:data
                                              encoding:NSUTF8StringEncoding];
    if(isScanning) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(onTick:) object:nil];
        [self stopScan];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        DDLogInfo(@"Scanned Barcode: %@", barcode);
        
//        UIAlertController *alertController = [UIAlertController
//                                              alertControllerWithTitle:@"Information"
//                                              message:[NSString stringWithFormat:@"Scanned Barcode: %@", barcode]
//                                              preferredStyle:UIAlertControllerStyleAlert];
//        //We add buttons to the alert controller by creating UIAlertActions:
//        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
//                                                           style:UIAlertActionStyleDefault
//                                                         handler:^(UIAlertAction *action)
//                                   {
//                                       [self search:barcode];
//                                   }];
//        [alertController addAction:actionOk];
//        [self presentViewController:alertController animated:YES completion:nil];
        [self search:barcode];
    } else {
        DDLogInfo(@"Scanned Barcode: %@ but ignoring", barcode);
    }
}

-(void)startScan {
    isScanning = YES;
    [[VFDevice barcode] startScan];
    
    [[VFDevice barcode] setScanner2D];
    [[VFDevice barcode] setScanTimeout:5000];
    [[VFDevice barcode] setLevel];
    [[VFDevice barcode] setBeepOn];
    
    [[VFDevice barcode] includeAllBarcodeTypes];
    [[VFDevice barcode] barcodeTypeEnabled:TRUE];
    
    [self performSelector:@selector(onTick:) withObject:nil afterDelay:5.1];
}

-(void)stopScan {
    isScanning = NO;
    
    [[VFDevice barcode] setBeepOff];
    [[VFDevice barcode] sendTriggerEvent:NO];
    [[VFDevice barcode] abortScan];
}

-(void)onTick:(NSTimer *)timer {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self stopScan];
}

- (void)myProgressTask {
    // This just increases the progress indicator in a loop
    float progress = 0.0f;
    while (progress < 1.0f) {
        progress += 0.01f;
        HUD.progress = progress;
        usleep(50000);
    }
}

-(void)textFieldDidChange :(UITextField *)theTextField{
    if ([[theTextField text] length] > 0) {
        [_goBtn setEnabled:YES];
        [_goBtn setBackgroundColor:[UIColor redColor]];
    } else {
        [_goBtn setEnabled:NO];
        [_goBtn setBackgroundColor:[UIColor grayColor]];
    }
}

@end
