//
//  ProductList.m
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/29/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//

#import "ProductListController.h"
#import "ProductListCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+AFNetworking.h"
#import "ProductDetailController.h"

@implementation ProductListController

@synthesize searchResults, searchString;

#pragma mark -
#pragma mark UICollectionViewDataSource

-(void)viewDidLoad {
    [super viewDidLoad];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.collectionView setCollectionViewLayout:flowLayout];
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    [flowLayout setItemSize:CGSizeMake((screenWidth)/3, 229)];
    
    [self setTitle:[NSString stringWithFormat:@"Search Results for \"%@\"", searchString]];
}

-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    NSArray* itms = searchResults.results.catalogSearch.itemList.items;
    return [itms count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductListCell *productCell = [collectionView
                                    dequeueReusableCellWithReuseIdentifier:@"ProductCell"
                                    forIndexPath:indexPath];
    
    Item* item = [searchResults.results.catalogSearch.itemList.items objectAtIndex:indexPath.row];
    
    NSString* imageURL = [NSString stringWithFormat:@"%@/%@",[item.primaryInformation imageLocation], [item.primaryInformation imageID]];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imageURL]];
    UIImage *placeholderImage =[UIImage imageNamed:@"gc-logo-transparent"];
    __weak ProductListCell *weakProductCell = productCell;
    [productCell.imageView setImageWithURLRequest:request
                              placeholderImage:placeholderImage
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                           
                                           weakProductCell.imageView.image = image;
                                           [weakProductCell setNeedsLayout];
                                           
                                       } failure:nil];
    productCell.itemIdLbl.text = [item displayItemId];
    productCell.productNameLbl.text = [item.primaryInformation shortDescription];
    productCell.unitOfMeasureLbl.text = [item unitOfMeasure];
    productCell.wasPriceLbl.text = @"Not Available";
    NSString* priceStr;
    if(item.primaryInformation.unitCost != nil) {
       priceStr = [NSString stringWithFormat:@"$%@", item.primaryInformation.unitCost];
       productCell.nowPriceLbl.text = priceStr;
    } else {
       productCell.nowPriceLbl.text = @"Not Available";
    }
    
    productCell.layer.borderWidth=1.0f;
    productCell.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    return productCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main"
                                                 bundle:nil];
    ProductDetailController *pdCtrl  = [sb instantiateViewControllerWithIdentifier:@"ProductDetailController"];
    Item* item = [searchResults.results.catalogSearch.itemList.items objectAtIndex:indexPath.row];
    [pdCtrl setItem:item];
    [self.navigationController pushViewController:pdCtrl animated:YES];
}


@end
