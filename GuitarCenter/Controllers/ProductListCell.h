//
//  ProductListCellCollectionViewCell.h
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/29/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductListCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *itemIdLbl;
@property (strong, nonatomic) IBOutlet UILabel *productNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *unitOfMeasureLbl;
@property (strong, nonatomic) IBOutlet UILabel *wasPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *nowPriceLbl;

@end
