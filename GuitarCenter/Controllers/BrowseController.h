//
//  FirstViewController.h
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/21/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <VMF/VMFramework.h>

@interface BrowseController : UIViewController <VFIBarcodeDelegate>
@property (weak, nonatomic) IBOutlet UIButton *goBtn;
@property (weak, nonatomic) IBOutlet UITextField *searchTextFld;
- (IBAction)scanTapped:(id)sender;
- (IBAction)goTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *goButton;



@end

