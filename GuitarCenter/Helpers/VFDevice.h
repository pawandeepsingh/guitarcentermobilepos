//
//  VFDevice.h
//  Singleton Class
//
#import    <VMF/VMFramework.h>
#import     <ExternalAccessory/ExternalAccessory.h>

@interface VFDevice:NSObject
+ (VFIPinpad *) pinPad;
+ (VFIBarcode *) barcode;
+ (VFIControl *) control;
@end
