//
//  Logger.h
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/24/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//



#ifndef GuitarCenter_Logger_h
#define GuitarCenter_Logger_h

#if defined(__OBJC__)

#import <CocoaLumberjack/DDLog.h>
#import <CocoaLumberjack/CocoaLumberjack.h>
extern int ddLogLevel;

#endif

#endif
