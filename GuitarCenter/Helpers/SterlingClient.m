//
//  SterlingClient.m
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/31/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//

#import "SterlingClient.h"
#import "SearchResultsModel.h"

@implementation SterlingClient

+ (NSDictionary *)modelClassesByResourcePath {
    return @{
             @"storeui/api/store/api/*": [SearchResultsModel class]
             };
}

@end
