//
//  VFDevice.m
//  VMF Device Sample
//
//  Created by James Gnann on 1/10/14.
//  Copyright (c) 2014 VeriFone, Inc. All rights reserved.
//

#import "VFDevice.h"

@implementation VFDevice
+ (VFIPinpad *) pinPad{
    static VFIPinpad *_pinPad = nil;
    if (_pinPad == nil) {
        _pinPad = [[VFIPinpad alloc] init];
    }
    return _pinPad;
}
+ (VFIBarcode *) barcode{
    static VFIBarcode *_barcode = nil;
    if (_barcode == nil) {
        _barcode = [[VFIBarcode alloc] init];
    }
    return _barcode;
}
+ (VFIControl *) control{
    static VFIControl *_control = nil;
    if (_control == nil) {
        _control = [[VFIControl alloc] init];
    }
    return _control;
}
@end
