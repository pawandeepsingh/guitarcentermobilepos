//
//  Logger.m
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/24/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//
#import "Logger.h"

#ifdef DEBUG
  int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
  int ddLogLevel = LOG_LEVEL_WARN;
#endif
