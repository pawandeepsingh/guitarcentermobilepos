//
//  AppDelegate.h
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/21/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

