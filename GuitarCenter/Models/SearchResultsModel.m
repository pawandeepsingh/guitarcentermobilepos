//
//  SearchResultsModel.m
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/31/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchResultsModel.h"

@implementation CategoryModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"shortDescription": @"ShortDescription",
             @"categoryPath": @"CategoryPath",
             @"desc": @"Description",
             @"count": @"Count",
             @"sequenceNo": @"SequenceNo"
             };
}

@end

@implementation CategoryList

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"categories": @"Category"
             };
}
+ (NSValueTransformer *)categoriesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:CategoryModel.class];
}
@end

@implementation PrimaryInformation

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"isDeliveryAllowed": @"IsDeliveryAllowed",
             @"isParcelShippingAllowed": @"IsParcelShippingAllowed",
             @"isEligibleForShippingDiscount": @"IsEligibleForShippingDiscount",
             @"isReturnService": @"IsReturnService",
             @"costCurrency": @"CostCurrency",
             @"isPickupAllowed": @"IsPickupAllowed",
             @"serializedFlag": @"SerializedFlag",
             @"isReturnable": @"IsReturnable",
             @"displayItemDescription": @"DisplayItemDescription",
             @"extendedDisplayDescription": @"ExtendedDisplayDescription",
             @"desc": @"Description",
             @"imageID": @"ImageID",
             @"taxableFlag": @"TaxableFlag",
             @"isShippingAllowed": @"IsShippingAllowed",
             @"isAirShippingAllowed": @"IsAirShippingAllowed",
             @"isStandaloneService": @"IsStandaloneService",
             @"imageLocation": @"ImageLocation",
             @"shortDescription": @"ShortDescription",
             @"unitCost": @"UnitCost",
             @"isHazmat": @"IsHazmat"
             };
}
+ (NSValueTransformer *)unitCostJSONTransformer {
    return [MTLValueTransformer transformerUsingReversibleBlock:^id(NSObject* obj, BOOL *success, NSError *__autoreleasing *error) {
        if (success) {
            if ([obj isKindOfClass:NSString.class]) {
                return (NSString*)obj;
            } else if ([obj isKindOfClass:NSNumber.class]){
                NSNumber *dblVal = (NSNumber*)obj;
                return [NSString stringWithFormat:@"%g",[dblVal doubleValue]];
            } else {
                return @"100.00";
            }
        }else{
            return @"";
        }
    }];
}

@end

@implementation Item

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"itemID": @"ItemID",
             @"unitOfMeasure": @"UnitOfMeasure",
             @"itemKey": @"ItemKey",
             @"primaryInformation": @"PrimaryInformation",
             @"displayItemId": @"DisplayItemId"
             };
}

+ (NSValueTransformer *)primaryInformationJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:PrimaryInformation.class];
}

@end

@implementation ItemList

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"items": @"Item"
             };
}
+ (NSValueTransformer *)itemsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:Item.class];
}

@end

@implementation CategoryDomain

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"shortDescription": @"ShortDescription",
             @"desc": @"Description",
             @"categoryDomain": @"CategoryDomain",
             @"attributeName": @"AttributeName",
             @"isClassification": @"IsClassification"
             };
}

@end

@implementation SortField

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"indexFieldName": @"IndexFieldName"
             };
}

@end

@implementation SortFieldList

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"sortFields": @"SortField"
             };
}
+ (NSValueTransformer *)sortFieldsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:SortField.class];
}

@end

@implementation CatalogSearch

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"pageNumber": @"PageNumber",
             @"facetList": @"FacetList",
             @"categoryDomain": @"CategoryDomain",
             @"callingOrganizationCode": @"CallingOrganizationCode",
             @"itemList": @"ItemList",
             @"sortFieldList": @"SortFieldList",
             @"pageSize": @"PageSize",
             @"spellingSuggestions": @"SpellingSuggestions",
             @"categoryList": @"CategoryList",
             @"totalPages": @"TotalPages",
             @"categoryDepth": @"CategoryDepth",
             @"totalHits": @"TotalHits"
             };
}

+ (NSValueTransformer *)categoryListJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:CategoryList.class];
}
+ (NSValueTransformer *)sortFieldListJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:SortFieldList.class];
}
+ (NSValueTransformer *)itemListJSONTransformer {
    return [MTLValueTransformer transformerUsingReversibleBlock:^id(NSObject* obj, BOOL *success, NSError *__autoreleasing *error) {
        if (success) {
            if ([obj isKindOfClass:NSDictionary.class]) {
                NSDictionary* dict = (NSDictionary*)obj;
                NSError *err = nil;
                MTLJSONAdapter *jsonAdapter = nil;
                if([[dict valueForKey:@"Item"] isKindOfClass:NSDictionary.class]) {
                    ItemList* itemList = [[ItemList alloc] init];
                    jsonAdapter = [MTLJSONAdapter modelOfClass:[Item class] fromJSONDictionary:[dict valueForKey:@"Item"] error:&err];
                    Item* item = (Item*)jsonAdapter;
                    itemList.items = [NSArray arrayWithObject:item];
                    return itemList;
                } else if ([[dict valueForKey:@"Item"] isKindOfClass:NSArray.class]) {
                    jsonAdapter = [MTLJSONAdapter modelOfClass:[ItemList class] fromJSONDictionary:dict error:&err];
                } else {
                    
                }
                return jsonAdapter;
            } else {
                return nil;
            }
        }else{
            return @"";
        }
    }];
}
+ (NSValueTransformer *)categoryDomainJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:CategoryDomain.class];
}

@end

@implementation Results

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"catalogSearch": @"CatalogSearch"
             };
}

+ (NSValueTransformer *)catalogSearchJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:CatalogSearch.class];
}

@end

@implementation SearchResultsModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"message": @"message",
             @"status": @"status",
             @"results": @"results"
             };
}
+ (NSValueTransformer *)resultsJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:Results.class];
}

@end
