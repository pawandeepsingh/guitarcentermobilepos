//
//  SearchResultsModel.h
//  GuitarCenter
//
//  Created by Pawandeep Singh on 12/31/15.
//  Copyright © 2015 Expicient Inc. All rights reserved.
//
#import "Mantle.h"


@class CategoryModel;

@interface CategoryList : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSArray<CategoryModel*> *categories;

@end

@interface CategoryModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *shortDescription;
@property (nonatomic, copy, readonly) NSString *categoryPath;
@property (nonatomic, copy, readonly) NSString *desc;
@property (nonatomic, copy, readonly) NSNumber *count;
@property (nonatomic, copy, readonly) NSNumber *sequenceNo;

@end

@interface PrimaryInformation : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *isDeliveryAllowed;
@property (nonatomic, copy, readonly) NSString *isParcelShippingAllowed;
@property (nonatomic, copy, readonly) NSString *isEligibleForShippingDiscount;
@property (nonatomic, copy, readonly) NSString *isReturnService;
@property (nonatomic, copy, readonly) NSString *costCurrency;
@property (nonatomic, copy, readonly) NSString *isPickupAllowed;
@property (nonatomic, copy, readonly) NSString *serializedFlag;
@property (nonatomic, copy, readonly) NSString *isReturnable;
@property (nonatomic, copy, readonly) NSString *displayItemDescription;
@property (nonatomic, copy, readonly) NSString *extendedDisplayDescription;
@property (nonatomic, copy, readonly) NSString *desc;
@property (nonatomic, copy, readonly) NSString *imageID;
@property (nonatomic, copy, readonly) NSString *taxableFlag;
@property (nonatomic, copy, readonly) NSString *isShippingAllowed;
@property (nonatomic, copy, readonly) NSString *isAirShippingAllowed;
@property (nonatomic, copy, readonly) NSString *isStandaloneService;
@property (nonatomic, copy, readonly) NSString *imageLocation;
@property (nonatomic, copy, readonly) NSString *shortDescription;
@property (nonatomic, copy, readonly) NSNumber *unitCost;
@property (nonatomic, copy, readonly) NSString *isHazmat;

@end

@interface Item : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *itemID;
@property (nonatomic, copy, readonly) NSString *unitOfMeasure;
@property (nonatomic, copy, readonly) NSString *itemKey;
@property (nonatomic, strong, readonly) PrimaryInformation *primaryInformation;
@property (nonatomic, copy, readonly) NSString *displayItemId;

@end

@interface ItemList : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSArray<Item*> *items;

@end

@interface CategoryDomain : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *shortDescription;
@property (nonatomic, copy, readonly) NSString *desc;
@property (nonatomic, copy, readonly) NSString *categoryDomain;
@property (nonatomic, copy, readonly) NSString *attributeName;
@property (nonatomic, copy, readonly) NSString *isClassification;

@end

@interface SortField : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *indexFieldName;

@end

@interface SortFieldList : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSArray<SortField*> *sortFields;

@end

@interface CatalogSearch : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *pageNumber;
@property (nonatomic, copy, readonly) NSString *facetList;
@property (nonatomic, strong, readonly) CategoryDomain *categoryDomain;
@property (nonatomic, copy, readonly) NSString *callingOrganizationCode;
@property (nonatomic, strong, readonly) ItemList* itemList;
@property (nonatomic, strong, readonly) SortFieldList* sortFieldList;
@property (nonatomic, copy, readonly) NSNumber *pageSize;
@property (nonatomic, copy, readonly) NSString *spellingSuggestions;
@property (nonatomic, strong, readonly) CategoryList *categoryList;
@property (nonatomic, copy, readonly) NSNumber *totalPages;
@property (nonatomic, copy, readonly) NSNumber *categoryDepth;
@property (nonatomic, copy, readonly) NSNumber *totalHits;

@end

@interface Results : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong, readonly) CatalogSearch *catalogSearch;

@end

@interface SearchResultsModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *message;
@property (nonatomic, copy, readonly) NSNumber *status;
@property (nonatomic, strong, readonly) Results *results;

@end


