# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: iOS Native app for demo to GC. This demo will contain
    * Search based on a keyword or code. Results need to be displayed as product list/product details.
    * Scan a product bar code and take the user to a product detail page.
    * Payment related demo - show the capability of the verifone sled.
* Version : 1.0

### How do I get set up? ###

* Summary of set up
   * git clone git@bitbucket.org:<username>/guitarcentermobilepos.git
   * got to the project directory
   * Open the workspace file in the project.
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Contact detail for the owner pawandeep.singh@expicient.com
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)